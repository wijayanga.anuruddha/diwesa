#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/cv.h>
#include <iostream>
#include <math.h> 
#include <windows.h>
#include <sapi.h>
#include "voice.h"

using namespace cv;

Mat edges;
	int threshold_value = 0;
	int threshold_type = 3;;
	int const max_value = 255;
	int const max_type = 4;
	int const max_BINARY_value = 255;

	char* trackbar_type = "Type: \n 0: Binary \n 1: Binary Inverted \n 2: Truncate \n 3: To Zero \n 4: To Zero Inverted";
	char* trackbar_value = "Value";
	void Threshold_Demo(int, void*);

class obstacle {

public:
	
int edge(){

			{
				VideoCapture cap(0); // open the default camera
				if (!cap.isOpened())  // check if we succeeded
					return -1;

				int counter = 0;
				namedWindow("edges", 1);
				int past = -1;
				for (;;)
				{
					Mat frame;
					cap >> frame; // get a new frame from camera
					cvtColor(frame, edges, CV_BGR2GRAY);
					GaussianBlur(edges, edges, Size(7, 7), 1.5, 1.5);
					Canny(edges, edges, 0, 30, 3);
					
					//threshold(edges, edges, 128, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
					threshold(edges, edges, 80, 255, CV_THRESH_BINARY);

					resize(edges, edges, Size(900, 500), 0, 0, INTER_CUBIC);
					Mat outputImg_left = edges(Rect(0, 0,300, 500));
					Mat outputImg_mid = edges(Rect(300, 0, 300, 500));
					Mat outputImg_right = edges(Rect(600, 0, 300, 500));

					int detecting_threshold = 2000; //divide from 2000 pixcell as base  
					int threshold_pRange = 6; //divide from 2000 pixcell as base
					
					//printf("left :%d mid :%i right :%i \n", (int)countNonZero(outputImg_left) / detecting_threshold, countNonZero(outputImg_mid) / detecting_threshold, countNonZero(outputImg_right) / detecting_threshold);
					int left_pixl = (int)countNonZero(outputImg_left) / detecting_threshold;
					int mid_pixl = (int)countNonZero(outputImg_mid) / detecting_threshold;
					int right_pixl = (int)countNonZero(outputImg_right) / detecting_threshold;
					
					/*counter++;
					printf("d%", counter);
					if (counter%30==0){
						
					}*/
					
					int present = instruction(mid_pixl, right_pixl, left_pixl, threshold_pRange);
					if (past != present){
						past = present;
						voice_output(past);
					}

					imshow("edges", edges);
					//imshow("left", outputImg_left);
					//imshow("mid", outputImg_mid);
					//imshow("right", outputImg_right);


					if (waitKey(30) >= 0) break;
				}
				// the camera will be deinitialized automatically in VideoCapture destructor
				return 0;
			}
	}

	
	
	int instruction(int mid_pixl, int right_pixl, int left_pixl, int threshold_pRange){
		int a = -1;
		if (mid_pixl >= threshold_pRange){
			if (right_pixl <= threshold_pRange)
			{
				a = 0;//turn right
				printf("turn right\n");
			}
			else if (left_pixl <= threshold_pRange)
			{
				a = 1;//turn left
				printf("turn left\n");
			}
			else{
				a = 2;//stop
				printf("stop\n");
			}
		}
		else
		{
			a = 3;//go straight
			printf("go straight\n");
		}
		return a;
	}

	int voice_output(int i){
	
	ISpVoice * pVoice = NULL;	

		if (FAILED(::CoInitialize(NULL)));
		HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);

		if (SUCCEEDED(hr)){
				if (i == 0)hr = pVoice->Speak(L"hello deewesa", 0, NULL);
				if (i == 1)hr = pVoice->Speak(L"test 1", 0, NULL);
						}
			::CoUninitialize();
	}
};


int main()
{
	
	obstacle diwesa;
	diwesa.edge();
	
}